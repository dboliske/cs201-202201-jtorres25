package project;

class produceItem extends item
{
  //attributes
  private String expirationDate;
  //constructors
  public produceItem(String n, double p, String d)
  {
     super(n, p);
     expirationDate = d;
  }
  //methods
  public void setExpirationDate(String d)
  {
     expirationDate = d;
  }
  public String getExpirationDate()
  {
     return expirationDate;
  }
  public String getInfo()
  {
     return "Name: " + getName() + "\nPrice: " + getPrice() + "\nExpiration Date: " + expirationDate;
  }
}
