package project;

class ageRestrictedItem extends item
{
  //attributes
  private int ageRestriction;
  //constructors
  public ageRestrictedItem(String n, double p, int a)
  {
     super(n, p);
     ageRestriction = a;
  }
  //methods
  public void setAgeRestriction(int a)
  {
     ageRestriction = a;
  }
  public int getAgeRestriction()
  {
     return ageRestriction;
  }
  public String getInfo()
  {
     return "Name: " + getName() + "\nPrice: " + getPrice() + "\nAge Restriction: " + ageRestriction;
  }
}
