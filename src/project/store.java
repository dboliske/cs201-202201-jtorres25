package project;

import java.util.Scanner;
import java.util.*;
import java.io.*;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.File;
import java.io.FileWriter;

class store
{
	  public static void main(String args[]) throws FileNotFoundException
	  {
	     Scanner keyboard = new Scanner(System.in);
	     System.out.println("Welcome to the store!"); // these next 3 lines greets the user and once somehthing is entered it will proceed to show the menu
	     System.out.println("What can I help you find today?");
	     String input = keyboard.nextLine();
	     int choice = 0; // this will assit in adding the ability to choose 
	     while(choice != 6) // This gives the user the multiple options to choose from. 
	     {
	        System.out.println("Menu:");
	        System.out.println("1. Add Item");
	        System.out.println("2. Sell Item");
	        System.out.println("3. Search for Item");
	        System.out.println("4. Modify Item");
	        System.out.println("5. Show Inventory");
	        System.out.println("6. Exit");
	        System.out.println("Your choice?");
	        choice = keyboard.nextInt(); // this will record the choice that was made
	        if(choice == 1) // when choosing 1 add item it will give the user another set of choices as well as asking them to choose from them
	        {
	           System.out.println("What type of item is this?");
	           System.out.println("1. Produce");
	           System.out.println("2. Shelved");
	           System.out.println("3. Age Restricted");
	           int type = keyboard.nextInt();
	           if(type == 1) // if user chooses produce it will ask for the name price and expiration date
	           {
	              System.out.println("Name?"); // ask the user for the items name to be recorded
	              String name = keyboard.next();
	              System.out.println("Price?"); // asks the user for the price to be recorded
	              double price = keyboard.nextDouble();
	              System.out.println("Expiration date?"); // asks the user for the expiration date to be recorded
	              String date = keyboard.next();
	              item newItem = new produceItem(name, price, date);
	              addItem(newItem);
	           }
	           else if(type == 2) // if the user chose shelve it will only ask for name and price
	           {
	              System.out.println("Name?"); // ask the user for the items name to be recorded
	              String name = keyboard.next();
	              System.out.println("Price?"); // asks the user for the price to be recorded
	              double price = keyboard.nextDouble();
	              item newItem = new shelvedItem(name, price);
	              addItem(newItem);
	           }
	           else if(type == 3) // if the user chose age restricted it will ask for name price and what age it will be restricted from
	           {
	              System.out.println("Name?"); // ask the user for the items name to be recorded
	              String name = keyboard.next();
	              System.out.println("Price?"); // asks the user for the price to be recorded
	              double price = keyboard.nextDouble();
	              System.out.println("Age restriction?"); // asks the user for the age requirement to be sold and then records it
	              int age = keyboard.nextInt();
	              item newItem = new ageRestrictedItem(name, price, age);
	              addItem(newItem);
	           }
	           else // this will help if the user types something that doesnt work with the data given
	           {
	              System.out.println("Invalid type");
	           }
	        }
	        else if(choice == 2) // this will ask the user to type in the name of the item they want to purchose
	        {
	           System.out.println("What would you like to buy? (name)");
	           String name = keyboard.next();
	           sellItem(name);
	        }
	        else if(choice == 3)
	        {
	           System.out.println("What are you looking for?"); // this will ask the user the name of the item they are looking for and see if it is in the inventory
	           String name = keyboard.next();
	           searchForItem(name);
	        }
	        else if(choice == 4)
	        {
	           System.out.println("What would you like to modify?"); // this will ask the user the name of the item they want to motify
	           String name = keyboard.next();
	           modifyItem(name);
	        }
	        else if(choice == 5) // this should show the user what is in the csv file
	        {
	        	 System.out.println("");
	        	 Scanner sc = new Scanner(new File("src/project/stock.csv"));
	        	 sc.useDelimiter("");
	        	 while (sc.hasNext()) {
	        	      System.out.print(sc.next());
	        	    }
	        	 System.out.println(""); // gives a bit of space between the the listed inventory and the menu
	        	 System.out.println("");
	        }
	        else if(choice == 6) // this will exit the user out of the program if they choose the choice 6
	        {
	           System.out.println("Thank you for shopping at the store!");
	        }
	        else
	        {
	           System.out.println("Invalid choice"); // if the user types something that isnt one of the choices it will repeat itself
	        }
	     }
	  }
	  public static void addItem(item newItem)
	  {
	     // add item to inventory
	  }
	  public static void sellItem(String name)
	  {
	     //sell item from inventory
	  }
	  public static void searchForItem(String name)
	  {
	     //search for item in inventory
	  }
	  public static void modifyItem(String name)
	  {
	     //modify item in inventory
	  }
	  public static void showInventory()
	  {
	     //show all items in inventory
	  }
	}