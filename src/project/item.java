package project;

	public abstract class item
	{
	  //attributes
	  private String name;
	  private double price;
	  //constructors
	  public item(String n, double p)
	  {
	     name = n;
	     price = p;
	  }
	  //methods
	  public String getName()
	  {
	     return name;
	  }
	  public double getPrice()
	  {
	     return price;
	  }
	  public void setName(String n)
	  {
	     name = n;
	  }
	  public void setPrice(double p)
	  {
	     price = p;
	  }
	  public abstract String getInfo();
	}
