package project;

class shelvedItem extends item
{
  //constructors
  public shelvedItem(String n, double p)
  {
     super(n, p);
  }
  //methods
  public String getInfo()
  {
     return "Name: " + getName() + "\nPrice: " + getPrice();
  }
}
