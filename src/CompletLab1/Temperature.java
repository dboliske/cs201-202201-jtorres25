package CompletLab1;

import java.util.Scanner;

public class Temperature {

	public static void main(String[] args) {
		// Jonathan Torres
		// CS 201
		// Section 1
		// 1/24/2021
		
		Scanner input = new Scanner(System.in); // This here will create a scanner
		
		System.out.println("What is the temperature in Fahrenheit? "); // This will ask the user to type the temperature in Fahrenheit 
		
		String FTemperature = input.nextLine(); // This will allow the user to type in a word or number (better number only)
		
		double FResult = (((Double.valueOf(FTemperature) - 32) * 5) / 9); // this converts the string to a double then into Celsius
		
		System.out.print("The temperature in celsius is " + ((double)(FResult)) + "C."); // This will show the user the temperature in Celsius
		
		
		
		
		
		System.out.print(" What is the temperature in Celsius? "); // This will ask the user to type the temperature in Celsius 
		
		String CTemperature = input.nextLine(); // This will allow the user to type in a word or number (better number only)
		
		double CResult = (((Double.valueOf(CTemperature) * 9) / 5) + 32); // this converts the string to a double then into Fahrenheit
		
		System.out.println("The temperature in Fahrenheit is " + ((double)(CResult)) + "F."); // This will show the user the temperature in Celsius
		
		input.close(); // This will close the scanner

	}

}
