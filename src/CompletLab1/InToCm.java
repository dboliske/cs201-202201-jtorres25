package CompletLab1;

import java.util.Scanner;

public class InToCm {

	public static void main(String[] args) {
		// Jonathan Torres
		// CS 201
		// Section 1
		// 1/24/2021
		
		Scanner input = new Scanner(System.in); // This here will create a scanner
		
		System.out.println("Tpye in the amount of inches you would like to convert. "); // This will ask the user how many inches they want to convert
		
		String Inches = input.nextLine(); // This will allow the user to type in a word or number for the length (better number only)
		
		double Cm = (Double.valueOf(Inches)) * 2.54; // This will turn the string into a double and then convert the number of inches to centimeters
		
		System.out.println("The amount of inches you typed in centimeters is " + ((double)(Cm)) + "cm.");
		
		input.close(); // This will close the scanner

	}

}
