package CompletLab1;

import java.util.Scanner;

public class AskName {

	public static void main(String[] args) {
		// Jonathan Torres
		// CS 201
		// Section 1
		// 1/24/2021
		
		Scanner input = new Scanner(System.in); // This here will create a scanner
		
		System.out.print("What is your name? "); // This will ask the user's name
		
		String word = input.nextLine(); // This will record what the user to typed in
		
		System.out.print("Your name is " + ((String)(word)) + "."); // This will show "Your name is : " and the users name then it adds a period in the end to make it a complete sentence
		
		input.close(); // This will close the scanner
		
	}

}
