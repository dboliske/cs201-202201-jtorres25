package CompletLab1;

public class Calculations {

	public static void main(String[] args) {
		// Jonathan Torres
		// CS 201
		// Section 1
		// 1/24/2021
		
		int DadAge = 52; // My Dad's age
		int MyAge = 20; // My age
		int SubAge = DadAge - MyAge; // My dad's age minus my age
		
		int BirthYear = 2001; // This is the year I was born
		int Year = BirthYear * 2; // This is my birth year multiplied by 2
		
		double height = 70.8; // My height in inches
		double CmHeight = height * 2.54; // If you multiply an inch by 2.54 it'll convert to centimeters 
		
		double FtHeight = height / 12; // Using the code from line 14 it converts my height from inches to feet
		
		System.out.println("My dad's age minus my age is " + ((int)(SubAge)) + "."); // This tells the user the difference between my age and my dads
		
		System.out.println("My birth year multiplied by 2 is " + ((int)(Year)) + "."); // This tells the user my birth year if it was multiplied by 2
		
		System.out.println("My height in centimeters is " + ((double)(CmHeight)) + "cm."); // This tells the user my height in centimeters
		
		System.out.println("My height in inches is " + ((double)(height)) + "in and my height in feet is " + ((double)(FtHeight)) + "ft."); // This tells the user my height in feet and inches
		

	}

}
