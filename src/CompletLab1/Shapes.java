package CompletLab1;

import java.util.Scanner;

public class Shapes {

	public static void main(String[] args) {
		// Jonathan Torres
		// CS 201
		// Section 1
		// 1/24/2021

		Scanner input = new Scanner(System.in); // This here will create a scanner
		
		System.out.println("What is the length in inches? "); // This will ask the user to the length of the box
		
		String Length = input.nextLine(); // This will allow the user to type in a word or number for the length (better number only)
		
		System.out.println("What is the width in inches? "); // This will ask the user to the width of the box
		
		String Width = input.nextLine(); // This will allow the user to type in a word or number for the width (better number only)
		
		System.out.println("What is the height in inches? "); // This will ask the user to the height of the box
		
		String Height = input.nextLine(); // This will allow the user to type in a word or number for the height (better number only)
		
		double box = (((Double.valueOf(Length)) * (Double.valueOf(Width))) * (Double.valueOf(Height))); // This will multiply the length. height and width that were typed in
		
		System.out.println("Using the width of " + ((String)(Width)) + " inches, the length of " + ((String)(Length)) + " inches and the height of " + ((String)(Height)) + " inches."); // This will say all the dimensions that were written
		
		System.out.println("The amount of wood needed is " + ((double)(box)) + " ft^2."); // This will tell the user the dimension of the box 
		
		
		input.close(); // This will close the scanner
		
	}

}
