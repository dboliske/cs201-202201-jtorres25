package CompletLab1;

import java.util.Scanner;

public class Initals {

	public static void main(String[] args) {
		// Jonathan Torres
		// CS 201
		// Section 1
		// 1/24/2021
		
		Scanner input = new Scanner(System.in); // This begins the scanner 
		
		System.out.print("What is your name. "); // This asks the user for their name to type
		
		char c = input.nextLine().charAt(0); // This will read the users name and grab the first letter
		
		System.out.print("Your name's intital is " + ((char)(c)) + "."); // This will tell the user their first initial 
		
		input.close(); // This will close the scanner

	}

}
