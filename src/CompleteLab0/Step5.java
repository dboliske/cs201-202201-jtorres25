package CompleteLab0;

public class Step5 {

	public static void main(String[] args) {
		// Jonathan Torres
		//CS 201
		// Section 1
		// 1/19/2021
		
		// I am assuming we are tasked to create a literal square 
		// The fist step would be to create the top half of the square
		// You'll want to type "System.out.println("*********");" 
		// After that you'll want to make the sides of the square
		// You'll want to type "System.out.println("*-------*");" you might want to do this 3 times to make nice square in the end
		// Finally you type the same thing you did for the top part of the square

		System.out.println("*********");
		System.out.println("*-------*");
		System.out.println("*-------*");
		System.out.println("*-------*");
		System.out.println("*********");
		
		// Judging by the results
		// The square came out successfully 
	}

}
