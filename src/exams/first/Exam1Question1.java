package exams.first;

import java.util.Scanner;

public class Exam1Question1 {

	public static void main(String[] args) {
		
		int integer = 0;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Insert an integer ");
		
		int a= sc.nextInt();
		
		int result = a + 65;
		
		char c = char.valueOf(int result); // the idea was to convert the integer to a character
		
		System.out.print("This lead to " + ((char)(c)) + ".");
		
		sc.close();
		
		

	}

}
