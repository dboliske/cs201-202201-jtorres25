# Midterm Exam

## Total

64/100

## Break Down

1. Data Types:                  18/20
    - Compiles:                 4/5
    - Input:                    5/5
    - Data Types:               4/5
    - Results:                  5/5
2. Selection:                   6/20
    - Compiles:                 4/5
    - Selection Structure:      2/10
    - Results:                  0/5
3. Repetition:                  16/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               1/5
4. Arrays:                      11/20
    - Compiles:                 4/5
    - Array:                    1/5
    - Exit:                     5/5
    - Results:                  1/5
5. Objects:                     13/20
    - Variables:                5/5
    - Constructors:             2/5
    - Accessors and Mutators:   3/5
    - toString and equals:      3/5

## Comments

1. Close, but all you needed was to have `(char)result` and it would have been cast correctly.
2. A good start, but missing most of the implementation.
3. A good start, but loop bounds are off and missing brackets are causing some logic issues.
4. Again, started, but having issues with assigning and accessing values from the array.
5. Default constructor doesn't assign any values, the non-default constructor and mutator do not validate `age`, and the `equals` method does not follow the UML with an Object parameter nor correctly compare `names`.
