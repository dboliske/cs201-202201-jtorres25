package exams.second;

import java.util.*;
import java.io.*;

public class Code4 {    
    static void selectionSort(String arr[],int n)
    {
        for(int i = 0; i < n - 1; i++) // The code has a function that sorts using select sort. It takes an array and the array length as its parameters and loops through the array given.
        	                           // It check each value to the next in the array and sorts in ascending order and returns the sorted array
        {
            int min_index = i;
            String minStr = arr[i];
            for(int j = i + 1; j < n; j++)
            {
                if(arr[j].compareTo(minStr) < 0)
                {
                    minStr = arr[j];
                    min_index = j;
                }
            }
            
            if(min_index != i)
            {
                String temp = arr[min_index];
                arr[min_index] = arr[i];
                arr[i] = temp;
            }
        }
    }

    public static void main(String args[]) // The main function takes the string array passed and prints it before it was sorted then passes the array to the selectionSort function where it gets sorted and prints it
    {
        String arr[] = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"}; // this is the string array
        int n = arr.length; 
        System.out.println("Given array is");

        for(int i = 0; i < n; i++)
        {
            System.out.println(i+": "+arr[i]);
        }
        System.out.println();

        selectionSort(arr, n);

        System.out.println("Sorted array is");
        
        for(int i = 0; i < n; i++)
        {
            System.out.println(i+": "+arr[i]);
        }
    }
}
