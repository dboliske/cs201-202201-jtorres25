package exams.second;

public class ComputerLab {

	boolean computers; // the first part of the uml diagram

    public ComputerLab() { // beginning of the other part of the uml diagram with the the public class of ComputerLab
    }

    public void setComputers(boolean computers) { // have setComputer prepared as a public boolean becasue of the + and (computers:boolean)
        this.computers = computers;
    }
    
    public boolean hasComputers() { // have hasComputers prepated as public boolean
        return computers;
    }

    @Override
    public String toString() { // create a toString and have it be public
        return "ComputerLab{" + "computers=" + computers + '}';
    }       
}
