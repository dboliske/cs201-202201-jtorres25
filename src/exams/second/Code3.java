package exams.second;

import java.util.ArrayList;
import java.util.Scanner;


public class Code3 {

public static void main(String[] args) {
        
    Scanner scan = new Scanner(System.in); //creates scanner
       
    ArrayList<Integer> numbers = new ArrayList<Integer>(); //creates an ArrayList
    
    String input; // This will hold the inputs from scanner
    int inputNum; // This will hold the input if integer
        
        System.out.println("Input the numbers into the sequance: " ); //This will ask the user to type in the numbers 
        
        do{                         //this loop will only stop when input is "Done"
            input = scan.next();    //save scanned input into input variable
            
        try{
             inputNum = Integer.parseInt(input); // This will converts the input into integer
             numbers.add(inputNum); // This will add the inputs into a ArrayList numbers
            }
        catch (NumberFormatException ex){  // This is an exception when input is not integer
             //ex.printStackTrace();   
                    
            int min = numbers.get(0);
            // This will loop to find minimum from ArrayList
            for (int i = 1; i< numbers.size(); i++) {
                  if (numbers.get(i) < min) { // This will compare every element of ArrayList to the first element
                            min = numbers.get(i); // This is now the minimum number in the ArrayList
                  }
             }
             int max = numbers.get(0);
             // This will loop to find maximum from ArrayList
             for (int i = 1; i< numbers.size(); i++) {
                  if (numbers.get(i) > max) { // This will compare every element of ArrayList to the first element
                      max = numbers.get(i); // This is now the maximum number in the ArrayList
                      }
                  }                    
                    
               System.out.println(min + "Is the minimum "); // This will show the user the minimum
               System.out.println(max + "Is the maximum "); // This will show the user the maximum
           }
            
        }
        while (input != "Done"); // When "Done" is typed in the loop will end and the promps will be displayed

    }
}
