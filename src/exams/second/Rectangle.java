package exams.second;

//sub class
//used keyword "extend" to inherits the properties of polygon

public class Rectangle extends Polygon {
	private double width; // the first part of the uml diagram that is set at private because of the -
	private double height;
	
	public Rectangle() { // begining of the other part of the uml diagram and it is set as public
		super();
		width = 1;
		height = 1;
	}
	
	public void setWidth(double width) { // this prepares setWidth as a public double because of the + and the (width:double)
		this.width = width;
	}
	
	public void setHeight(double height) { // this prepares setHeight as a public double because of the + and the (height:double)
		this.height = height;
	}
	
	public double getWidth() { // this has getWidth set as a public double to accompany setWidth
		return width;
	}
	
	public double getHeight() { // this has getHeight set as a public double to accompany setHeight
		return height;
	}
	
	@Override
	public String toString() { // have toString prepared as a string 
		return "[Name: " + getName() + " Width: " + width + " Height: " + height + "]";
	}
	
	@Override
	public double area() { // Both area and perimeter overide the polygon and are public double
		return height * width;	
	}
	
	@Override
	public double perimeter() {
		return 2.0 * (height + width);
	}
}