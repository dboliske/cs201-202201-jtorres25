package exams.second;

public class Polygon {
	private String name; // the first part of the uml diagram for polygons  is a private string
	
	public Polygon() { // begining of the other part of the uml diagram with polygon being a public class
		name = "shape";
	}
	
	public void setName(String name) { // have setname be prepared as a public string becasue of the + and (name:string)
		this.name = name;
	}
	
	public String getName() { // have getName be prepared as public string to accompany setName
		return name;
	}
	
	public String toString() { // have toString prepared as a string
		return "Name: " + name ;
	}
	
	//empty function declarations
	//sub classes will override with their implementation
	public double area() {
		return 0;	
	}
	
	public double perimeter() {
		return 0;
	}
}
