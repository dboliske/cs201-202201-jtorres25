package exams.second;

public class Classroom {

	String roomnumber;
	String building; // this is the inital part of the uml diagram
    
    int seats; 

    public Classroom() { // begining of the other part of the uml diagram + measn public
    } 

    public void setbuilding(String building) { // have setbuilding as a public string because of the + and (number:string)
        this.building = building;
    }

    public void setroomnumber(String roomnumber) { // have setroomnumber as a public string becasue of the + and (number:string)
        this.roomnumber = roomnumber;
    }

    public void setseats(int seats) { // have setseats as a public int
        if (seats < 0){ 
            throw new IllegalArgumentException("Seats cannot be lower than 0");
        }else{
            this.seats = seats;
        }
    }
    
    public String getbuilding() { // turn getbuilding as a public to accompany setbuilding
        return building;
    }
    
    public String getroomnumber() { // have getroomnumber be a public string to accompany setroomnumber
        return roomnumber;
    }
    
    public int getseats() { // have getseats as a public int becasue of the + and (seats:int)
        return seats;
    }

    @Override
    public String toString() { // have the toString be public and handle a string over the seats
        if (seats < 0){ 
            throw new IllegalArgumentException("Seats cannot be lower than 0");
        }
        return "Classroom{" + "building=" + building + ", roomNumber=" + roomnumber + ", seats=" + seats + '}';
    }
}
