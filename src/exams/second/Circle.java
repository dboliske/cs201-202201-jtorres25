package exams.second;

//sub class
//used keyword "extend" to show that circle inherits
//the properties of polygon
public class Circle extends Polygon {
	private double radius; // the first part of the uml diagram that is set at private because of the -
	
	public Circle() { // begining of the other part of the uml diagram and it is set as public
		super();
		radius = 1;
	}
	
	public void setRadius(double width) { // setRadius is prepared as public double because of the + and (width:double)
		radius = width;
	}
	
	public double getRadius() { // this prepares getRadius as a public double to accompany setRadius
		return radius;
	}
	
	@Override
	public String toString() { // have toString prepared as a string
		return "Name: " + getName() + " Radius: " + radius;
	}
	
	@Override
	public double area() {  // Both area and perimeter overide the polygon and are public double 
		return Math.PI * radius * radius;	
	}
	
	@Override
	public double perimeter() {
		return 2.0 * Math.PI * radius ;
	}
}
