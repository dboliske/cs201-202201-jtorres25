package labs.lab6;

public class GenericQueue<E> {

	//Jonathan Torres
	// CS 201
	// Section 1
	// 3/21/2022
	
	 private java.util.LinkedList<E> list = new java.util.LinkedList<E>();
     public void enqueue(E e){
         list.addLast(e);
     }
     public E dequeue(){
         return list.removeFirst();
     }
     public int getSize(){
         return list.size();
     }
     public String toString(){
         return "Queue; " + list.toString();
     }
}