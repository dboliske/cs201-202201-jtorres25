# Lab 1

## Total

16.5/20

## Break Down

* Exercise 1    2/2
* Exercise 2    2/2
* Exercise 3    2/2
* Exercise 4
  * Program     2/2
  * Test Plan   0/1
* Exercise 5
  * Program     1.5/2
  * Test Plan   0/1
* Exercise 6
  * Program     2/2
  * Test Plan   0/1
* Documentation 5/5

## Comments
- Please include test plans
- Ex.5 output unit should be converted to sq ft
- Please edit the lab in Eclipse instead of creating a new project called CompleteLab