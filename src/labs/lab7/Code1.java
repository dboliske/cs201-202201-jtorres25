package labs.lab7;

public class Code1 {

	//Jonathan Torres
	// CS 201
	// Section 1
	// 4/4/2022
	
	 static void bubbleSort(int[] arr) { 
	        //assigning array length to variable n
	        int n = arr.length;  
	        //Declaring temp variable for swapping
	        int temp = 0; 
	        //first loop is for how many pass
	         for(int i=0; i < n; i++){ 
	             /*second loop is for comparsion for each pass every time 
	             comparsion will become less because as pass increase some of Array
	             is already sorted that's why loop run up to (n-i)*/
	                 for(int j=1; j < (n-i); j++){  
	                     //compare previous elements with next element 
	                          if(arr[j-1] > arr[j]){  
	                                 //if previous element is greater then swapping is being  
	                                 temp = arr[j-1];  
	                                 arr[j-1] = arr[j];  
	                                 arr[j] = temp;  
	                         }  
	                          
	                 }  
	         }  
	  
	    }  
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int arr[] ={10,4,7,3,8,6,1,2,5,9};  
        //printing array before sort
        System.out.println("Array Before Bubble Sort");  
        for(int k=0; k < arr.length; k++){  
                System.out.print(arr[k] + " ");  
        }  
        System.out.println();  
        //calling bubbleSort  
        bubbleSort(arr);//sorting array elements using bubble sort  
        //printing array after sort
        System.out.println("Array After Bubble Sort");  
        for(int k=0; k < arr.length; k++){  
                System.out.print(arr[k] + " ");  
        }  

}  
}