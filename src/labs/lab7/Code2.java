package labs.lab7;

public class Code2 {

	//Jonathan Torres
	// CS 201
	// Section 1
	// 4/4/2022
	
	public static void main(String[] args) {
		
		String[] arr = {"cat", "fat", "dog", "apple", "bat", "egg"};
		
		int count = 0;
		
		String sortedArray[] = sort_sub(arr,arr.length); // capture length of array
		
		for(int i=0; i<sortedArray.length;i++){
			
			System.out.println(sortedArray[i]);
			
		}

	}



public static String[] sort_sub(String array[], int k) {
	
	String temp=""; // Temporary string movement variable
	
	for(int i=0; i<k; i++){ // loop to run in every pass
		
		for(int j=i+1; j<k; j++){ // Loop for passes
			
			if(array[i].compareToIgnoreCase(array[j])>0) { // Condition to swap position if true
				
				temp = array[i];
				
				array[i] = array[j];
				
				array[j] = temp;
			}
		}
	}
return array; // Return sorted array
}
}
