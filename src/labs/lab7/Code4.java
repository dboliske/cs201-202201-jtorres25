package labs.lab7;

import java.util.Scanner;

public class Code4 {

	//Jonathan Torres
	// CS 201
	// Section 1
	// 4/4/2022
	
	public static void main(String[] args) {
        // string array
        String a[]={"c","html","java","python","ruby","scala"};
        System.out.println("Array of string ");

        // printing array of string
        for (String s : a) 
            System.out.print(s+" ");
        //creating a scanner object
        Scanner sc=new Scanner(System.in);
        // User inputing word to search
        System.out.print("\nEnter word to Search: ");
        String word=sc.nextLine();
        // function calling
        System.out.println(binarySearch(a,word,0,a.length));
    }
    // binary search function
    public static int binarySearch(String[] words, String word,int start,int end)  {
   //calculating the mid
    int mid =(start+end )/ 2;

    //comparing word with the mid
    int comp=word.compareTo(words[mid]);

    // recursion termminating condition
    // start index is greater than end
    if(end < start) return -1;
    // if word is found
    else if(comp==0)   return mid;
    // if word is less than mid
    else if(comp > 0) return binarySearch(words, word,mid+1,end);
    //if word is greater than the mod
    else    return binarySearch(words,word,start,mid-1);

    }   
}
