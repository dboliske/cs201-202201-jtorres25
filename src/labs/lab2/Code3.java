package labs.lab2;

import java.util.Scanner;

public class Code3 {

	public static void main(String[] args) {
		
		// Jonathan Torres
		// CS 201
		// Section 1
		// 1/31/2021

	    Scanner input = new Scanner(System.in); // This opens a scanner

	    char operator;   // Lines 14 and 15 are variables
	    Double result;
	    
	    System.out.println("Choose an option: 1, 2, 3, or 4"); // This asks the user for a choice of 1 to 4
	    
	    operator = input.next().charAt(0); // This records what the user types

	    switch (operator) { // This line uses switch cases to allow the user to make choices

	      
	    case '1': // This line triggers if the user types "1"
	    	  
	        System.out.println("Hello"); // This will tell the user "Hello"
	        
	        break; // This breaks the case
	        
	    
	      case '2': // This line triggers if the user types "2"
	    	  
	        Double number1, number2; // These are the variables that were used
	        
	        System.out.println("Enter first number"); // This asks the user for the first number to add
	        
	        number1 = input.nextDouble(); // This records the first number as "number1"

	        System.out.println("Enter second number"); // This number asks the user for the second number to add
	        
	        number2 = input.nextDouble(); // This records the second number as "number2"
	        
	        result = number1 + number2; //  This adds the 2 numbers together to get the result and puts it into the variable "result"
	        
	        System.out.println(number1 + " + " + number2 + " = " + result); // This displays what was added and the results to the user
	        
	        break; // This breaks the case

	        
	      case '3': // This line triggers if the user types "1"
	    	  
	        Double number3, number4; // These are the variables that will be used
	        
	        System.out.println("Enter first number");  // This asks the user for the first number to multiply
	        
	        number3 = input.nextDouble(); // This records the first number as "number3"

	        System.out.println("Enter second number"); // This asks the user for the second number to multiply
	        
	        number4 = input.nextDouble(); // This records the second number as "number4"
	        
	        result = number3 * number4; //  This multiplys the 2 numbers together to get the result and puts it into the variable "result"
	        
	        System.out.println(number3 + " * " + number4+ " = " + result); // This displays what was multiplied and the results to the user
	        
	        break; // This breaks the case
	        
	        
	      case '4':
	      
	    	  System.out.println("Goodbye"); // This will tell the user "Goodbye" 
		        
		        break; // This breaks the case

	      default:
	    	  System.out.println("This is not one of the choices."); // Lines 78 to 79 tells the user that they didn't enter one of the choices
	    	  System.out.println("Please Try Again");
	    	  
	    }

	    input.close(); // This closes the scanner
	  }
	}