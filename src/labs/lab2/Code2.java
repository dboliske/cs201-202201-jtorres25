package labs.lab2;

import java.util.Scanner;

public class Code2 {

	public static void main(String[] args) {
		
		// Jonathan Torres
		// CS 201
		// Section 1
		// 1/31/2021
		
		 Scanner sc = new Scanner(System.in); // This opens a scanner  
		 
	        int total = 0, n, count = 0; // These are the initial variables 
	        
	        while  (true) {
	            System.out.print("Enter grade (-1 to finish entering): "); // This will ask the user for a grade from 0 to 100 and suggests that -1 will finish the entering
	            
	            n = sc.nextInt(); // this records the variable that was entered as "n"
	            
	            if (n == -1) { //This line that breaks the boolean if the user entered -1
	         
	                break; // This is the break
	            }
	            total += n; // This will use the amount of "n" that was entered and make it the total
	            
	            count++; // This count how many inputs were add and basically is the adding 100 for every "n" (cause 100 is full points)
	        }
	        float average = total / count; // This grabs the total and divides it by the count to get the average and puts it in the variable "average"
	        
	        System.out.println("Average: " + average); // This tells the user the average
	        
	        sc.close(); // closes the scanner
	    }
	}