# Lab 2

## Total

18/20

## Break Down

* Exercise 1    6/6
* Exercise 2    5/6
* Exercise 3    5/6
* Documentation 2/2

## Comments
-Ex2 doesn't handle decimals
-Ex3 doesn't run repeatedly until user enters 4