package labs.lab2;

import java.util.Scanner;

public class Code1 {

	public static void main(String[] args) {
		
		// Jonathan Torres
		// CS 201
		// Section 1
		// 1/31/2021
		
		Scanner sc= new Scanner(System.in);    // This opens a scanner  
		
		System.out.println("Please enter the dimension: ");  // This Will ask the user for the dimension
		
		int a= sc.nextInt();  // This will take the next line that the user types as a value for "a"

		for(int i=0;i<a;i++) // This will loop for each row for "a" times
		   {
		      for(int j=0;j<a;j++){   // This will loop for each Column for "a" times
		    	  
		            System.out.print("* "); // This line places the "*" for the square
		         }
		      
		      System.out.println(); // This allows for a new line after each row
		      
		      sc.close(); // closes the scanner
		   }
		   }  
		}  



