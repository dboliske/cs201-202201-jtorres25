package labs.lab4;

public class ApplicationForPhoneNumber {

	// Jonathan Torres
	// CS 201
	// Section 1
	// 2/21/2021

	
	// application class that instantiates two instances of PhoneNumber
	

		public static void main(String args[]) {
			
			//use the default constructor
			PhoneNumber pn1 = new PhoneNumber();
			// use mutator methods to set its values
			pn1.setCountryCode("02");
			pn1.setAreaCode("123");
			pn1.setNumber("7777777");
			
			//use the non-default constructor
			PhoneNumber pn2 = new PhoneNumber("02","4156", "9191999");
			
			//not part of the requirements, just to test validation of area code and number
			System.out.println("PhoneNumber 1 area code valid: "+pn1.validateAreaCode());
			System.out.println("PhoneNumber 1 number valid: "+pn1.validateNumber());
			System.out.println("PhoneNumber 2 area code valid: "+pn2.validateAreaCode());
			System.out.println("PhoneNumber 2 number valid: "+pn2.validateNumber());
			
			
			// Display the values of each object by calling the toString method
			System.out.println(pn1.toString());
			System.out.println(pn2.toString());
			
			
		}
	}

