package labs.lab4;

import java.util.*;

public class Potion {

	// Jonathan Torres
	// CS 201
	// Section 1
	// 2/21/2021


		 //Variables declarations..
		  private String name;
		  private double strength;
		  //Default constructor..
		  public Potion(){
		    this.name = null;
		    this.strength = 0.0;
		  }
		  //Non-Default constructor..
		  public Potion(String name,double strength){
		    this.name =name;
		    this.strength = strength;
		  }
		  //two accessor methods..
		  public String getName(){
		    return name;
		  }
		  public double getStrength(){
		    return strength;
		  }
		  //Two mutator methods..
		  public void setName(String name){
		    this.name = name;
		  }
		  public void setStrength(double strength){
		    this.strength = strength;
		  }
		  //toString method..
		  public String toString(){
		    return "Name of potion is "+name+" and its strength is "+strength;
		  }
		  //Valid strength method definition..
		  public boolean validStrength(double strength){
		    if(strength > 0 && strength <= 10 )
		    {
		      return true;
		    }
		    else
		    {
		      return false;
		    }
		  }
		  //Equals method definition..
		  public boolean equals(Potion p){
		    if(this.name == p.name && this.strength == p.strength)
		    {
		      return true;
		    }
		    else
		    {
		      return false;
		    }
		  }
		}
