package labs.lab4;

public class ApplicationForPotion {

	// Jonathan Torres
	// CS 201
	// Section 1
	// 2/21/2021

	
	//Starting point of execution..
	  public static void main(String[] args) {
	    //Two object declaration and initialization..
	    Potion p1 = new Potion("Magic",4.5);
	    Potion p2 = new Potion();
	    p2.setName("Zoombie");
	    p2.setStrength(6.5);
	    //checking for equals method..
	    if(p1.equals(p2) == true)
	    {
	      System.out.println("Both potions are same.");
	    }
	    else
	    {
	      System.out.println("Both potions are different.");
	    }
	    if(p2.validStrength(9.5) == true)
	    {
	      System.out.println("Valid strength of potion");
	    }
	    else
	    {
	      System.out.println("Invalid strength of potion");
	    }
	    //printing output..
	    System.out.println("Details of Potions :");
	    System.out.println(p1.toString());
	    System.out.println(p2.toString());
	  }
	}
