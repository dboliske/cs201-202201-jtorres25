package labs.lab4;

public class PhoneNumber {

	// Jonathan Torres
	// CS 201
	// Section 1
	// 2/21/2021

		
		// three instance variables
		private String countryCode;
		private String areaCode;
		private String number;

		// default constructor
		public PhoneNumber() {

		}

		// non-default constructor
		public PhoneNumber(String countryCode, String areaCode, String number) {
			this.countryCode = countryCode;
			this.areaCode = areaCode;
			this.number = number;
		}

		// 3 accessor methods
		public String getCountryCode() {
			return countryCode;
		}

		public String getAreaCode() {
			return areaCode;
		}

		public String getNumber() {
			return number;
		}

		// 3 mutator methods
		public void setCountryCode(String countryCode) {
			this.countryCode = countryCode;
		}

		public void setAreaCode(String areaCode) {
			this.areaCode = areaCode;
		}

		public void setNumber(String number) {
			this.number = number;
		}

		//method that will return the entire phone number as a single string (the toString method)
		@Override
		public String toString() {
			return countryCode + areaCode + number;
		}

		
		//method that will return true if the areaCode is 3 characters long
		//Optional: if the areaCode is 3 digit characters long
		public boolean validateAreaCode() {
			
			//check if not 3 chars long
			if (areaCode.length() != 3) {
				
				//return false
				return false;
			}

			try {
				//convert area code to Integer
				//is successful conversion, valid
				int i = Integer.parseInt(areaCode);
			} catch (NumberFormatException e) {
				
				//encounters error in conversion, invalid
				System.out.println("Invalid area code");
				return false;
			}
			return true;
		}

		
		//method that will return true if the number is 7 characters long.
		// Optional: if the number is 7 digit characters long
		public boolean validateNumber() {
			//check if not 7 chars long
			if (number.length() != 7) {
				return false;
			}

			try {
				//convert to number to Integer
				//is successful conversion, valid
				int i = Integer.parseInt(number);
			} catch (NumberFormatException e) {
				//encounters error in conversion, invalid
				System.out.println("Invalid number");
				return false;
			}
			return true;
		}

	}