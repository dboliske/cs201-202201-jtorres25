package labs.lab4;

public class GeoLocation {
	
	// Jonathan Torres
	// CS 201
	// Section 1
	// 2/21/2021

	private double lat;
	private double lng;
	  
	   //Default constructor. It means it takes no arguments.
	   //if we didn't create this constructor explicitly.then by default java creates default constructor.
	
	  GeoLocation() {
	       super();
	   }
	  
	   //non-default constructor which takes lat and lng as arguments.
	   public GeoLocation(double lat, double lng) {
	       super();
	       this.lat = lat;
	       this.lng = lng;
	   }

	   //accessor method for lat
	   
	   public double getLat() {
	       return lat;
	   }
	   //mutator method for lat
	   public void setLat(double lat) {
	       this.lat = lat;
	   }
	  
	   //accessor method for lng
	   public double getLng() {
	       return lng;
	   }
	  
	   //mutator method for lng
	   public void setLng(double lng) {
	       this.lng = lng;
	   }
	  
	   //toString() method . It is used to print the object in desirec format
	   @Override
	
	   public String toString() {
	       return "("+lat + "," + lng + ")";
	   }
	  
	   //checks lat whether it is between -90 and 90.8 and return true it it is.
	   public boolean checkLat()
	   {
	       if(this.lat>=-90 && this.lat<=90.8)
	           return true;
	       return false;
	   }
	  
	   //checks lng whether it is between -180 and 180.9 and return true it it is.
	   public boolean checkLong()
	   {
	       if(this.lng>=-180 && this.lng<=180.9)
	           return true;
	       return false;
	   }
	   //optional method
	   public GeoLocation getGeoLocation(String coordinates)
	   {
	       String[] latlng=coordinates.split(","); //splitting string to get lat and lng
	       String strlat=latlng[0].substring(1); //getting lat
	       double lat=Double.parseDouble(strlat); //converting lat from string to double
	       String strlng=latlng[1].substring(1); //getting lng
	       double lng=Double.parseDouble(strlng); //converting lat from string to double
	       GeoLocation geoLocation=new GeoLocation(lat,lng); //creating GeoLocation object
	       return geoLocation; //returning object
	      
	   }
	  

	}
