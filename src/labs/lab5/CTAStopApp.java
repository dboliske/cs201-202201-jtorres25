package labs.lab5;

	//Jonathan Torres
	// CS 201
	// Section 1
	// 3/7/2021

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class CTAStopApp {

public CTAStation[] readFile(String filename) throws IOException {
 BufferedReader reader = new BufferedReader(new FileReader("src/labs/lab5/CTAStops.csv"));

 int size = 10;
 CTAStation[] stations = new CTAStation[size];

 int index = 0;
 String line = "";

 while ((line = reader.readLine()) != null) {
  String[] splitedLine = line.split(",");

  /* for Name */
  String name = splitedLine[0];

  /* for Latitude */
  String latitude = splitedLine[1];

  /* for Longitude */
  String longitude = splitedLine[2];

  /* for Location */
  String location = splitedLine[3];

  /* for Wheelchair */
  String wheelchair = splitedLine[4];

  /* for Open */
  String open = splitedLine[5];

  /* escape the label of the csv */
  if (!name.equalsIgnoreCase("name") && !latitude.equalsIgnoreCase("latitude")
    && !longitude.equalsIgnoreCase("longitude") && !location.equalsIgnoreCase("location")
    && !wheelchair.equalsIgnoreCase("wheelchair") && !open.equalsIgnoreCase("open")) {

   double lat = Double.valueOf(latitude);
   double lng = Double.valueOf(longitude);
   boolean hasWheelChair = wheelchair.equalsIgnoreCase("true") ? true : false;
   boolean isOpen = open.equalsIgnoreCase("true") ? true : false;

   CTAStation station = new CTAStation(name, lat, lng, location, hasWheelChair, isOpen);

   if (index == size) {
    size = size * 2; /* twice the size */
    CTAStation[] newStations = new CTAStation[size];

    /* copy the stations towards newStations with new size */
    for (int i = 0; i < index; i++) {
     newStations[i] = stations[i];
    }

    newStations[index++] = station;
    stations = newStations;

   } else {
    stations[index++] = station;
   }
  }
 }

 reader.close();
 return stations;
}

public void menu(CTAStation[] stations) {
 Scanner scan = new Scanner(System.in);

 while (true) {
  try {
   System.out.println("\n[1] Display Station Names");
   System.out.println("[2] Display Stations with/without Wheelchair access");
   System.out.println("[3] Display Nearest Station");
   System.out.println("[4] Exit");

   System.out.print("Enter choice: ");
   int choice = Integer.parseInt(scan.nextLine());

   if (choice == 1) {
    displayStationNames(stations);
   } else if (choice == 2) {
    displayByWheelChair(scan, stations);
   } else if (choice == 3) {
    displayNearest(scan, stations);
   } else if (choice == 4) {
    break;
   }

  } catch (Exception e) {

  }
 }
}

public void displayStationNames(CTAStation[] stations) {
 for (CTAStation station : stations) {
  System.out.println(station.getName());
 }
}

public void displayByWheelChair(Scanner input, CTAStation[] stations) {
 while (true) {
  System.out.print("Choose 'Y' or 'N': ");
  String choice = input.nextLine();

  if (choice.equalsIgnoreCase("y")) {
   boolean found = true;
   for (CTAStation station : stations) {
    if (station.hasWheelChair()) {
     found = false;
     System.out.println(station);
    }
   }

   if (!found) {
    System.out.println("No Found station with wheel chair");
   }

   return;

  } else if (choice.equalsIgnoreCase("n")) {
   boolean found = true;
   for (CTAStation station : stations) {
    if (!station.hasWheelChair()) {
     found = false;
     System.out.println(station);
    }
   }

   if (!found) {
    System.out.println("No Found station without wheel chair");
   }

   return;
  }
 }
}

public void displayNearest(Scanner input, CTAStation[] stations) {
 System.out.print("Enter latitude: ");
 double latitude = Double.valueOf(input.nextLine());
 System.out.print("Enter longitude: ");
 double longitude = Double.valueOf(input.nextLine());

 CTAStation nearestStation = null;
 double nearest = 0.0;

 for (CTAStation station : stations) {
  try {
   if (nearestStation == null && nearest == 0.0) {
    nearest = station.calcDistanceMiles(latitude, longitude);
    nearestStation = station;
   } else {
    double temp = station.calcDistanceMiles(latitude, longitude);
    if (nearest > temp) {
     nearest = temp;
     nearestStation = station;
    }
   }
  } catch (Exception e) {
   // ignore
  }
 }

 System.out.print("Nearest station:");
 System.out.print(nearestStation + "\n");
}

public static void main(String[] args) throws IOException {
 String filename = "src/labs/lab5/CTAStops.csv";
 CTAStopApp cta = new CTAStopApp();
 CTAStation[] stations = cta.readFile(filename);
 cta.menu(stations);
}
}