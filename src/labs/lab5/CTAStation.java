package labs.lab5;

	//Jonathan Torres
	// CS 201
	// Section 1
	// 3/7/2021

import java.util.Objects;

	public class CTAStation extends GeoLocation {

	private String name;

	private String location;

	private boolean wheelChair;

	private boolean open;

	public CTAStation() {
	 super();
	}

	public CTAStation(String name, double lat, double lng, String location, boolean wheelChair, boolean open) {
	 super(lat, lng);
	 this.name = name;
	 this.location = location;
	 this.wheelChair = wheelChair;
	 this.open = open;
	}

	public String getName() {
	 return name;
	}

	public void setName(String name) {
	 this.name = name;
	}

	public String getLocation() {
	 return location;
	}

	public void setLocation(String location) {
	 this.location = location;
	}

	public boolean hasWheelChair() {
	 return wheelChair;
	}

	public void setWheelChair(boolean wheelChair) {
	 this.wheelChair = wheelChair;
	}

	public boolean isOpen() {
	 return open;
	}

	public void setOpen(boolean open) {
	 this.open = open;
	}

	@Override
	public String toString() {
	 return "CTAStation [name=" + name + ", location=" + location + ", wheelChair=" + wheelChair + ", open=" + open
	   + ", latitude=" + getLat() + ", longitude=" + getLng() + "]";
	}

	@Override
	public boolean equals(Object obj) {
	 if (this == obj)
	  return true;

	 if (!super.equals(obj))
	  return false;

	 if (getClass() != obj.getClass())
	  return false;

	 CTAStation other = (CTAStation) obj;
	 return Objects.equals(location, other.location) && Objects.equals(name, other.name) && open == other.open
	   && wheelChair == other.wheelChair;
	}

	}
