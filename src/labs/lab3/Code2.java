package labs.lab3;

import java.util.*;

import java.io.FileWriter;

import java.io.IOException;

public class Code2 {

	public static void main(String[] args) {
		
		// Jonathan Torres
		// CS 201
		// Section 1
		// 1/31/2021
		
		
		int[] num=new int[100];//to store numbers up to 100
		
		Scanner sc= new Scanner(System.in);
		
		int i=0;// This counts the actual size of the array
		
		while(true)
		{
			System.out.println("Enter the number:"); // This asks the user for numbers 
		
			String a=sc.nextLine();//This reads the numbers that the users typed
		
			if(a.equals("Done")) // This will lead the the loop to breal if the user types "Done"
			{
				break;//if input is done loop breaks
			}
			else
			{
				num[i]=Integer.parseInt(a); //This converts the read string to number and adding to num array
			}
			i++;
		}
		
		System.out.println("Enter the Filename:"); // This asks the user for a file name 
		
		String fileName=sc.nextLine(); //This will allow the user to type a filename
		
		try
		{//try block	
			FileWriter writer = new FileWriter(fileName,true); //This is for opening the file
			for(int j=0;j<i;j++)
			{
				writer.write(num[j]+"\t"+""); // This is for writing to the file
			}
			writer.close();// This will close the file
		}
		catch (IOException e)
		{ //catch block
			e.printStackTrace();
			sc.close();
		}
		}
		

	}


