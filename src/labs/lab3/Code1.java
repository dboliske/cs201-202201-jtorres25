package labs.lab3;

import java.io.BufferedReader;

import java.io.FileReader;

import java.io.IOException;

public class Code1 {

	public static void main(String[] args) {
		
		// Jonathan Torres
		// CS 201
		// Section 1
		// 1/31/2021


		String line = "";

		String splitBy = ",";

		try {

		// parsing a CSV file into BufferedReader class constructor

		BufferedReader br = new BufferedReader(new FileReader("src/labs/lab3/grades.csv")); // This gets the file to read

		float sum=0 ;

		int count=0;

		while ((line = br.readLine()) != null) // This returns a Boolean value

		{

		String[] students = line.split(splitBy); // This uses a comma as a separator

		int marks = Integer.parseInt(students[1]); // get marks

		sum += marks; //calculates sum of marks

		count++; //count of total students

		}

		float avg = sum/count; // This gives the average

		System.out.println("Average grade = " + avg); //This tells the user the average

		} catch (IOException e) {

		e.printStackTrace();
		
		

		}

		}

		

	}


