package labs.lab3;

public class Code3 {

	public static void main(String[] args) {
		
		// Jonathan Torres
		// CS 201
		// Section 1
		// 1/31/2021
		
		// The line of code below are is the array 
		 int arr[] = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
		 
	     int  min; // This line creates the variable "min"
	        
	        min = arr[0]; // this has "min" ready to perform an action on the array
	        
	        for (int ele : arr) // This prepares the action
	        {
	            if(ele < min ) // This has it for if the value in lower
	            {
	                min = ele; // "min" will equal that lowered variable
	            }
	        }
	                System.out.println("Minimum element is: " + min); // This will show the user the minimum element of the array 
	        }
	}

	


